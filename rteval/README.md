# rteval

# rteval

`rteval` is a tool for running real-time tests pulled from the `rts-container-tooling` repository. It can measure various effects from things such as scheduling latencies, interrupt latencies, and overall system responsiveness under different workloads and configurations.

Running `rteval` inside a container provides a consistent and reproducible environment for real-time testing, independent of the host system's setup. The purpose of this container is to provide a consistent and reproducible environment for real-time testing, independent of the host system's setup. A user can use these tools to see if the realtime performance inside of the container matches the realtime performance of the underlying hard deadline requirements, and to show how much overhead the container runtime adds

## Prerequisites

- Ensure you have `podman` installed on your system.

## Installation and Usage

1. If you would like to test a local change, make sure you use the `--local-scratch` option in scripts/build-quay.sh as `root`
2. Run the container from quay:
```
sudo podman run -it --rm -privileged quay.io/rts-container-tooling/rteval:latest
``` 
3. You can mount storage for logs or sosreports using the `-v` flag. For example, to mount `/mnt/logs` and `/mnt/sosreports`, you can run:
```
sudo podman run -it --rm -privileged -v /mnt/logs:/mnt/logs -v /mnt/sosreports:/mnt/sosreports quay.io/rts-container-tooling/rteval:latest
```
4. You can run the local copy of the container with:
```
sudo podman run -it --rm -privileged -v /mnt/logs:/mnt/logs -v /mnt/sosreports:/mnt/sosreports rteval-next
```

5. After you launch the container succesfully, you should be dropped in an interactive shell where you can provide the desired rteval command to run or what CPU configuration we should use.Sometimes, defining the CPU configuration directly in rteval helps if the runtime is only reporting 1 cpu on the system. This makes it so that rteval only runs on the first CPU on the system. This can be fixed by setting both loads and measurement cpus to the same value. Here is an example for a 4 CPU single NUMA node system.
```
rteval --loads-cpulist=0-3 --measurement-cpu=0-3 -d 30s -v
```
