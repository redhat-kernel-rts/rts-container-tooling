# rts-container-tools

## Getting Started

Welcome to `rts-container-tools`, a comprehensive toolkit designed for container management with a focus on security and performance. This upstream project contains realtime packages maintained by the RTS (Real-Time Systems) team at Red Hat. 

Changes will be thoroughly tested here, and the resulting container images can be found at [https://quay.io/organization/rts-container-tooling](https://quay.io/organization/rts-container-tooling).

### Quick Start

To get started with `rts-container-tools`, follow these steps:

1. **Installation**:
   - Ensure you have Podman installed on your system.
   - Clone the repository:
   ```
   git clone https://github.com/RedHat-RTS/rts-container-tools.git
   ```
   - Navigate to the cloned repository:
    ```
   cd rts-container-tools
   scripts/build-quay.sh --local-scratch
    ```

## Project Purpose

The primary goal of `rts-container-tools` is to enhance container management by providing realtime performance tooling in containers. This empowers users to determine if their realtime requirements can be met within a containerized environment. 

Key focus areas include:

1. **Root Permissions**: These tools require root permissions in containers, which can pose a security risk. As such, they are intended to be deployed in environments for performance evaluation or debugging, where the container exists as a short-lived instance generating performance data.

2. **Performance Evaluations**: Our tools enable users to benchmark and analyze the realtime performance characteristics of their containerized applications.

3. **Seamless CI/CD**: We leverage GitLab's CI/CD framework for efficient testing and deployment of containerized applications.

Initially, `rts-container-tools` will officially support standalone containers. In the future, we plan to expand our offerings to cover more complex deployment scenarios.

By providing these tools and resources, we aim to help users confidently adopt containers for their realtime workloads, backed by the expertise of Red Hat's RTS team. However, it's crucial to understand the security implications of using these tools and to deploy them in appropriate, controlled environments.

## Test and Deploy

Our deployment strategy utilizes GitLab CI/CD, incorporating practices like Static Application Security Testing (SAST) and daily builds for both main and main-next. The pipeline uses a custom script located in scripts/build-quay.sh. Refer the the --help menu for more information. Detailed guidance is available in the [GitLab CI/CD documentation](https://docs.gitlab.com/ee/ci/).

## Development and Contribution

Contributions are highly encouraged! If you're looking to contribute, please read through this document and check out each components README.

### Development Workflow

- **Development Branch**: `main-next` serves as our development branch. All modifications, including Dockerfile updates and script adjustments, should be merged here initially.
- **Creating a Next Build**: To initiate a next build, generate a tag named `next-$CONTAINER_VER` and push it to `main-next`. This procedure ensures that nightly builds are triggered only when there's an active release in progress.
- **Releasing Code**: When you're set to release, create a Merge Request (MR) from `main-next` to `main`. Post-MR merger, execute the `--deprecate-next` option to finalize the release.
- **Daily Builds**: A daily build process checks for new versions of components on the stable `main` branch, automatically pushing new images upon detecting updates.

## License 
rts-container-tools is open-sourced under the GPL 2.0 license. Refer to our documentation for comprehensive license information.

## Project Status

rts-container-tools is actively developed, with frequent updates and additions.

This documentation serves as a starting point for understanding and engaging with rts-container-tools.

