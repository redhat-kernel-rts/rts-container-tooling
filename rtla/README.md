# RTLA Container

This container provides the `rtla` (Real-Time Linux Analysis) tool, including its `osnoise` and `timerlat` tracers, for analyzing and optimizing real-time performance in Linux systems. It is designed to measure scheduling latencies, timer-related delays, and system-induced noise, providing valuable insights into real-time system characteristics.

---

## Features

- **Real-Time Analysis**: Includes `osnoise` and `timerlat` tracers for evaluating real-time properties.
- **System Latency Measurement**: Measures and records scheduling latencies and timer interruptions.
- **Reproducible Environment**: Ensures consistent testing in a containerized setup.
- **Optimized for Debugging**: Focuses on diagnosing real-time performance issues.

---

## Tracer Overview

### **Osnoise Tracer**
The `osnoise` tracer measures operating system-induced latencies by tracking:
- Preemption
- Interrupts
- SoftIRQs

It provides insights into the disturbances affecting real-time workloads. Available modes:
- **Top Mode**: Displays periodic summaries of noise data.
- **Hist Mode**: Generates histograms for detailed analysis of noise samples.

### **Timerlat Tracer**
The `timerlat` tracer measures timer-related latencies by:
- Setting periodic timers on each CPU.
- Recording delays at both IRQ and thread handler levels.

Available modes:
- **Top Mode**: Displays real-time summaries of latency data.
- **Hist Mode**: Provides detailed histograms of latency events.

---

## Prerequisites

- Ensure you have `podman` or another compatible container runtime installed.

---

## Installation and Usage

### Pull and Run the Container

To run the container:
```bash
sudo podman run -it --rm --privileged quay.io/rts-container-tooling/rtla:latest