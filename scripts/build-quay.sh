#!/bin/bash
# Authored by: chwhite@redhat.com
# This script is used to build and push container images to quay.io
# It is intended to be run in a gitlab CI/CD pipeline

SCRIPT_PID=$$

trap_exit() {
    echo "Caught exit signal!"
    cd $DIR
    exit 1
}

# Function to print help menu
print_help() {
    echo "Usage: $0 --component COMPONENT_NAME [OPTIONS]"
    echo "--component        Name of the component (required, must be one of 'rteval', 'rtla', 'realtime-tests')"
    echo "--robot-user USER  Username for the Quay.io robot account (optional, defaults to 'rts-container-tooling')"
    echo "--robot-token TOKEN Token for the Quay.io robot account (optional, defaults to 'QUAY_ROBOT_TOKEN' environment variable)"
    echo "--tag TAG          Tag for the Docker image (optional, defaults to 'latest') [NOT TO BE USED IN PIPELINE]"
    echo "--image-org ORG    Docker image organization/repository (optional, defaults to 'rts-container-tooling/COMPONENT')"
    echo "--quay-build       Perform an official quay build for the official or next branch when set to true"
    echo "--force-push       Force push the container, overwriting the current latest build. [NOT TO BE USED IN PIPELINE]"
    echo "--force-push-prev  Force push the container, allowing you to push an older version of the image without overwriting the latest tag. [NOT TO BE USED IN PIPELINE]"
    echo "--next             Use 'next-' prefix for the Docker image tag and verify running from 'main-next' branch with existing git tag."
    echo "--deprecate-next   Deprecate the 'next-' tag by renaming it to 'next-VER-deprecated' in both Git and Quay."
    echo "--ssh-private-key  KEY_PATH  Path to the SSH private key (optional)."
    echo "--help             Print this help menu."
}

check_for_changes() {
    local branch=$1

    if ! git rev-parse --verify origin/$branch >/dev/null 2>&1; then
        echo "ERROR: origin/$branch branch does not exist. Please create it and try again."
        exit 1
    fi

    if ! git merge-base --is-ancestor origin/$branch HEAD; then
        echo "ERROR: Current HEAD is not based on origin/$branch. Please switch to a branch based on origin/$branch and try again."
        echo "INFO: If you are trying to build a next build, use the --next option"
        exit 1
    fi


    if [ "$(git rev-parse origin/$branch)" != "$(git rev-parse HEAD)" ]; then
        echo "ERROR: There are changes staged for commit or uncommitted changes. Please push your changes before proceeding. To perform a scratch build do not use --quay-build"
        exit 1
    elif [ -n "$(git status --porcelain)" ]; then
        echo "ERROR: There are changes staged for commit or uncommitted changes. Please stash your changes before proceeding. Or perform a To perform a scratch build do not use --quay-build"
        exit 1
    fi
}

setup_ssh_key() {
    if [ -z "$SSH_KEY" ]; then
        echo "WARNING: No SSH key provided. Using locally cached keys, which may not be the correct ones."
    else
        echo "Custom Private Key detected. Installing..."
        
        # Remove carriage returns that might have been added inadvertently
        SSH_KEY_CLEANED=$(echo "$SSH_KEY" | base64 --decode |tr -d '\r')

        if echo "$SSH_KEY_CLEANED" | grep -q "^-----BEGIN\ [A-Z]\+ PRIVATE\ KEY-----"; then
            echo "SSH key format looks correct."
        else
            echo "WARNING: SSH key format does not look correct. Please verify."
        fi

        # Create custom SSH directory    
        mkdir -p "$SSH_KEY_DIR"
        chmod 700 "$SSH_KEY_DIR"

        # Save the cleaned SSH key to a file
        echo "$SSH_KEY_CLEANED" > "$SSH_KEY_PATH"
        chmod 600 "$SSH_KEY_PATH"

        # Start the SSH agent and add the cleaned key
        eval "$(ssh-agent -s)"
        ssh-add "$SSH_KEY_PATH"

        # Set GIT_SSH_COMMAND for this context
        export GIT_SSH_COMMAND="ssh -i $SSH_KEY_PATH"
    fi
}


deprecate_next_tag() {
    if [ "$CRED_CHECK" = false ]; then
        echo "WARNING: You must be logged in to Quay.io to deprecate the next tag."
        exit 0
    fi

    if ! git checkout main; then
        echo "ERROR: --deprecate-next option requires being on 'main' branch."
        exit 1
    fi

    # Define the tags
    TAG="next-$CONTAINER_VER"
    DEPRECATED_TAG="${TAG}-deprecated"

    # 3. Check if the tag exists in Git
    if git rev-parse "$TAG" >/dev/null 2>&1; then
        # Tag exists in Git, proceed with deprecation
        git tag "$DEPRECATED_TAG" "$TAG"
        git push origin "$DEPRECATED_TAG"
        
        # Verify the deprecated tag was pushed successfully
        if git rev-parse "$DEPRECATED_TAG" >/dev/null 2>&1; then
            # Delete the old tag
            git tag -d "$TAG"
            git push origin --delete "$TAG"
            echo "Resetting origin/main-next to match main..."
            # run git diff between main and HEAD and only push and reset if there are **NOT** changes
            if [ "$(git rev-parse origin/main-next)" != "$(git rev-parse HEAD)" ]; then
                if [ -n "$(git status --porcelain)" ]; then
                    echo "ERROR: There are changes staged for commit or uncommitted changes. Please commit or stash your changes before proceeding."
                    exit 1
                fi
            else
                echo "No Changes Detected ... Resetting main-next to match main"
            fi

            if ! git checkout main-next; then
                echo "ERROR: Unable to checkout main-next branch."
                echo "INFO: Please manually reset main-next to match main."
                exit 1
            fi
            if ! git reset --hard origin/main; then
                echo "ERROR: Unable to reset main-next to main."
                echo "INFO: Please manually reset main-next to match main."
                exit 1
            fi
            if ! git push -f origin main-next; then
                echo "ERROR: Unable to force push main-next."
                echo "INFO: Please manually reset main-next to match main."
                exit 1
            fi
            echo "Successfully reset and pushed main-next to match main."
            git checkout main
        else
            echo "ERROR: Failed to push deprecated tag to Git."
            echo "INFO: You can try to run this again, but if it continues to fail, try manually to delete the tag and reset main-next branch"
            exit 1
        fi
    else
        echo "Nothing to deprecate in Git."
    fi

    # 3. Check if the tag exists in Quay
    TAG_EXISTS=$(curl -s "$TAG_REF" | jq -r ".tags[] | select(.name == \"$TAG\") | .name")
    if [ "$TAG_EXISTS" == "$TAG" ]; then
        DEPRECATED_TAG_EXISTS=$(curl -s "$TAG_REF" | jq -r ".tags[] | select(.name == \"$DEPRECATED_TAG\") | .name")
        if [ "$DEPRECATED_TAG_EXISTS" != "$DEPRECATED_TAG" ]; then
            if ! podman pull "$IMAGE:$TAG"; then
                echo "ERROR: Failed to pull the image $IMAGE:$TAG for deprecation."
                exit 1
            fi
            if ! podman tag "$IMAGE:$TAG" "$IMAGE:$DEPRECATED_TAG"; then
                echo "ERROR: Failed to tag the image for deprecation."
                exit 1
            fi
            if ! podman push "$IMAGE:$DEPRECATED_TAG"; then
                echo "ERROR: Failed to push the deprecated tag $IMAGE:$DEPRECATED_TAG."
                exit 1
            fi
        fi
        
        DEPRECATED_TAG_EXISTS=$(curl -s $TAG_REF | jq -r ".tags[] | select(.name == \"$DEPRECATED_TAG\") | .name")
        if [ "$DEPRECATED_TAG_EXISTS" == "$DEPRECATED_TAG" ]; then
            if ! skopeo delete "docker://$IMAGE:$DEPRECATED_TAG"; then
                echo "ERROR: Failed to delete the deprecated tag $IMAGE:$DEPRECATED_TAG in Quay."
                exit 1
            fi
            echo "Successfully deleted the deprecated tag: $DEPRECATED_TAG in Quay."
        else
            echo "ERROR: Failed to push deprecated tag to Quay."
            exit 1
        fi
    else
        echo "Nothing to deprecate in Quay."
    fi
    exit 0    
}

configure-next-build() {
    TAG="next-$CONTAINER_VER"
    DEPRECATED_TAG="${TAG}-deprecated"
    
    DEPRECATED_TAG_CHECK=$(curl -s $TAG_REF | jq -r ".tags[] | select(.name == \"$DEPRECATED_TAG\") | .name")
    if [ "$DEPRECATED_TAG_CHECK" == "$DEPRECATED_TAG" ]; then
        echo "WARNING: Tag $DEPRECATED_TAG already exists for $IMAGE."
        echo "This version of $COMPONENT has already been released."
        echo "If you are trying to build a new next build, make sure CONTAINER_VER is bumped in the dockerfile for the component you are working on."
        exit 0
    fi
    
    if ! git rev-parse "$TAG" >/dev/null 2>&1; then
        echo "WARNING: Git tag $TAG does not exist."
        echo "git tag $TAG"
        echo "git push origin "$TAG""
        echo "Please create this tag and try again."
        exit 0
    fi
    
    check_for_changes main-next
    
    git push origin main-next:$TAG -f
    FORCE_PUSH=true
}

setup_robot_credentials() {
    if [ -n "$ROBOT_USER" ] && [ -z "$ROBOT_TOKEN" ]; then
        echo "WARNING: --robot-token is required when --robot-user is provided."
        echo "INFO: No credentials provided for the robot account so no authentication will be attempted."
    elif [ -n "$ROBOT_TOKEN" ] && [ -z "$ROBOT_USER" ]; then
        echo "WARNING: --robot-user is required when --robot-token is provided."
        echo "INFO: No credentials provided for the robot account so no authentication will be attempted."
    elif [ -n "$ROBOT_USER" ] && [ -n "$ROBOT_TOKEN" ]; then
        if ! podman login -u="$ROBOT_USER" -p="$ROBOT_TOKEN" quay.io; then
            echo "ERROR: Unable to login to Quay.io with the provided robot credentials."
            exit 1
        fi
        CRED_CHECK=true
    fi
}

validate_component() {
    if [[ ! "$COMPONENT" =~ ^(rteval|rtla|realtime-tests)$ ]]; then
        echo "Error: --component is required and must be one of 'rteval', 'rtla', 'realtime-tests'."
        print_help
        exit 1
    fi

    if [ ! -f "$COMPONENT/Dockerfile" ]; then
        echo "WARNING: Dockerfile not found in $COMPONENT directory."
        echo "INFO: Run this script from the root of the repo 'scripts/build-quay.sh --component COMPONENT_NAME'"
        echo "INFO: --component must be one of 'rteval', 'rtla', 'realtime-tests'"
        echo "INFO: To add a custom Dockerfile, create a Dockerfile in the $COMPONENT directory."
        exit 0
    fi
}

build_and_push() {
    local component=$1
    local version_tag=$2
    # Define the manifest tags for multiarch build
    local manifest_tag="quay.io/${IMAGE_ORG}:${version_tag}"
    # Define a temporary tag to hold the current version's manifest
    # When we delete $TAG, it deletes the associated manifest as well
    # So we create a new manifest and push it to quay after cleanup.
    local manifest_temp="quay.io/${IMAGE_ORG}:${APP_CURR_VER}"

    # Define architectures to build
    local archs=("amd64" "arm64")  # Default to both architectures

    # Loop through architectures and build/push each
    for arch in "${archs[@]}"; do
        echo "Building for architecture: $arch"
        local image_tag=${manifest_tag}-${arch}
        if ! podman build --arch $arch -t "${image_tag}" .; then
            echo "ERROR: Failed to build the Docker image for architecture: $arch"
            exit 1
        fi

        if ! podman push "$image_tag"; then
            echo "ERROR: Failed to push the Docker image for architecture: $arch"
            exit 1
        fi
    done

    # Create and push manifest only if building for both architectures
    if [ "${#archs[@]}" -gt 1 ]; then
        # if $version_tag is $TAG, delete the manifest from quay
        if podman manifest rm "$manifest_tag" > /dev/null 2>&1; then
            echo "INFO: Manifest $manifest_tag deleted from local podman."
            echo "INFO: This steps cleans up previous builds"
        else
            echo "INFO: Manifest $manifest_tag does not exist locally."
        fi

        if podman rmi "$manifest_tag" > /dev/null 2>&1; then
            echo "INFO: Image $manifest_tag deleted from local podman."
        else
            echo "INFO: Image $manifest_tag does not exist locally."
        fi

        if [ "$version_tag" == "$TAG" ]; then
            if skopeo inspect "docker://${manifest_tag}" > /dev/null 2>&1; then
                echo "INFO: Manifest $manifest_tag already exists in Quay.io."
                # make a local copy of $manifest_temp as temp
                if podman manifest inspect "$manifest_temp" > /dev/null 2>&1; then
                    echo "INFO: Local manifest $manifest_temp already exists. Deleting..."
                    podman manifest rm "$manifest_temp"
                fi

                echo "Creating a temporary local manifest copy of $manifest_tag as $manifest_temp"
                podman manifest create "$manifest_temp"

                for arch in "${archs[@]}"; do
                    podman manifest add "$manifest_temp" "docker://${manifest_temp}-${arch}"
                done

                if ! skopeo delete "docker://${manifest_tag}"; then
                    echo "ERROR: Failed to delete manifest $manifest_tag from Quay.io and this step is needed to replace the old tag."
                    exit 1
                fi

                podman manifest push --all "$manifest_temp" "docker://${manifest_temp}"
            fi
        fi
        if ! podman manifest create "$manifest_tag"; then
            echo "ERROR: Failed to create manifest for $manifest_tag"
            exit 1
        fi
        for arch in "${archs[@]}"; do
            if ! podman manifest add "$manifest_tag" "docker://${manifest_tag}-${arch}"; then
                echo "ERROR: Failed to add $arch to manifest $manifest_tag"
                exit 1
            fi
        done
        if ! podman manifest push --all "$manifest_tag" "docker://${manifest_tag}"; then
            echo "ERROR: Failed to push manifest $manifest_tag"
            exit 1
        fi
    fi
}

LOCAL_SCRATCH=true
QUAY_BUILD=false
FORCE_PUSH_PREV=false
FORCE_PUSH=false
USE_NEXT=false
DEPRECATE_NEXT=false

# Parse command options and set variables
while [[ "$#" -gt 0 ]]; do
    case "$1" in
        --component) COMPONENT="$2"; shift ;;
        --robot-user) ROBOT_USER="$2"; shift ;;
        --robot-token) ROBOT_TOKEN="$2"; shift ;;
        --tag) TAG="$2"; shift ;;
        --image-org) IMAGE_ORG="$2"; shift ;;
        --quay-build) QUAY_BUILD=true ;;
        --force-push) FORCE_PUSH=true ;;
        --force-push-prev) FORCE_PUSH_PREV=true; FORCE_PUSH=true ;;
        --next) USE_NEXT=true ;;
        --deprecate-next) DEPRECATE_NEXT=true ;;
        --ssh-private-key) SSH_KEY="$2"; shift ;;
        --help) print_help; exit 0 ;;
        *) echo "Unknown option: $1"; print_help; exit 1 ;;
    esac
    shift
done

if [ "$QUAY_BUILD" = true ]; then
    echo "Running official build!!"
    LOCAL_SCRATCH=false
fi

DIR=$(pwd)

trap trap_exit SIGHUP SIGINT SIGQUIT SIGTERM

if [ "$USE_NEXT" = true ]; then
    if ! git checkout main-next; then
        echo "ERROR: --next option requires being on 'main-next' branch."
        exit 1
    fi
elif [ "$LOCAL_SCRATCH" = false ]; then
    # CHeck out main branch
    if ! git checkout main; then
        echo "ERROR: Building a official quay image requires being on 'main' branch."
        exit 1
    fi
fi

TAG=${TAG:="latest"}
IMAGE_ORG=${IMAGE_ORG:-"rts-container-tooling/$COMPONENT"}

CONTAINER_VER=$(cat $COMPONENT/Dockerfile | grep "container_version=" | cut -d'"' -f2)
IMAGE="quay.io/$IMAGE_ORG"
TAG_REF="https://quay.io/api/v1/repository/$IMAGE_ORG/tag/?onlyActiveTags=true"

FORCE_CONFIRM="I UNDERSTAND AND WANT TO CONTINUE"
CRED_CHECK=false

# SSH Private Key Check and Installation Logic
SSH_KEY_DIR="$HOME/.ssh_custom"
SSH_KEY_PATH="$SSH_KEY_DIR/id_rsa_custom"

#Verify building a valid component
validate_component

cd $COMPONENT

# Build a local copy for verification and version fetching logic
# Use naming convention of build_and_push so that the build is cached
if ! podman build --arch amd64 -t "$COMPONENT:$TAG-amd64" .; then
    echo "ERROR: Failed to build the Docker image $COMPONENT:$TAG-amd64"
    exit 1
fi
# Perform scratch build and exit for testing purposes. This is the default behavior
if [[ "$LOCAL_SCRATCH" == "true" ]]; then
    echo "Assuming there are no errors above, a local scratch copy of your current Dockerfile is provided at localhost/$TAG-amd64"
    echo "To test the container, run the following command: sudo podman run -it --rm --privileged $COMPONENT:$TAG-amd64"
    exit 0
fi


# Attempt to login into Quay
setup_robot_credentials

#Configure SSH Key for gitlab
setup_ssh_key

# Implementation of the --deprecate-next logic
if [ "$DEPRECATE_NEXT" = true ]; then
    deprecate_next_tag
fi

# Set Local build var
APP_NEXT_VER=$(podman run --rm "$COMPONENT:$TAG-amd64" rpm -q --queryformat '%{VERSION}-%{RELEASE}\n' $COMPONENT)
NEXT_TAG=$APP_NEXT_VER-$CONTAINER_VER

# New section to handle --next logic
if [ "$USE_NEXT" = true ]; then
    configure-next-build
else
    check_for_changes main
fi

echo "Changes are in sync with the target branch. Pulling the image..."

# Check if the image exist, if not - we want to force the build because nothing exist.
if podman pull "$IMAGE:$TAG"; then
    echo "Image $IMAGE:$TAG pulled successfully."
    # Set vars for versions
    APP_CURR_VER=$(podman run --rm "$IMAGE" rpm -q --queryformat '%{VERSION}-%{RELEASE}\n' $COMPONENT)
else
    echo "No existing image found. Setting up a new tag."
    APP_CURR_VER=0
    # Set to true so we create the new image.
    FORCE_PUSH=true
fi

echo "The current version of $COMPONENT in Quay.io is $APP_CURR_VER"
echo "The version of $COMPONENT in the local container build is $APP_NEXT_VER"

if [ "$CRED_CHECK" = false ]; then
    echo "No credentials provided for the robot account so no authentication will be attempted."
    echo "A test build has been performed. If there are no errors in the job, then basic sanity checks have passed and changes will be picked up in the next nightly build."
    exit 0
fi


if [ "$FORCE_PUSH" = false ]; then
    echo "To force push, run the script with the --force-push option."
    # Check for existing tags with NEXT_TAG
    TAG_EXISTS=$(curl -s "$TAG_REF" | jq -r ".tags[] | select(.name == \"$NEXT_TAG\") | .name")
    if [ "$TAG_EXISTS" == "$NEXT_TAG" ]; then
        echo "WARNING: Tag $NEXT_TAG already exists for $IMAGE."
        echo "There are no new versions of $COMPONENT or the dockerfile. Nothing to push..."
        exit 0
    fi
    echo "No matching tag found. Proceeding with the new build for $NEXT_TAG."
elif [ "$USE_NEXT" = false ] && [ "$APP_CURR_VER" != 0 ]; then
    echo "WARNING: THIS WILL OVERWRITE THE CURRENT LATEST BUILD AT $IMAGE:$TAG and $NEXT_TAG"
    echo "Confirm you action by typing the following prompt:"
    read -p "$FORCE_CONFIRM"": " confirm
    if [[ "$confirm" == "$FORCE_CONFIRM" ]]; then
        echo "Forcing push..."
        HIGHER_VERSION=$APP_NEXT_VER
        APP_CURR_VER=""
    else
        echo "Exiting. Do not want to force push. Rerun without this option."
        exit 1
    fi
fi

# Run final verification to ensure an older version from rteval is trying to be installed
# If we have gotten to this point, we have a new container image
# So we want to ensure that an older version is not accidently pushed over latest.
# "$APP_NEXT_VER" != "$APP_CURR_VER" to handle when the versions are the same but we have a new dockerfile
HIGHER_VERSION=$(printf "%s\n%s" "$APP_CURR_VER" "$APP_NEXT_VER" | sort -V | tail -n 1)
if [ "$HIGHER_VERSION" == "$APP_CURR_VER" ] && [ "$FORCE_PUSH" = false ] && [ "$APP_NEXT_VER" != "$APP_CURR_VER" ]; then
    echo "ERROR: An older version of $COMPONENT is trying to be installed. This should not be the case."
    echo "INFO: If you are trying to build a previous version of $COMPONENT, please use the --force-push-prev option."
    exit 1
fi

echo "Newer Version was detected!!"
echo ">> Tagging new version as "$IMAGE:$TAG" and pushing the new image."
if [ "$FORCE_PUSH_PREV" = false ]; then
    if ! build_and_push "$COMPONENT" "$TAG"; then
        echo "ERROR: Failed to tag the new version as $IMAGE:$TAG."
        exit 1
    fi    
    echo "Newer version updated, tagged, and pushed as: $IMAGE:$TAG"
fi

echo "Newer version updated, tagged, and pushed as: $IMAGE:$TAG"

if [ "$USE_NEXT" = false ]; then
echo ">> Tagging latest version as version: $NEXT_TAG"
    if ! build_and_push "$COMPONENT" "$NEXT_TAG"; then
        echo "Error: Failed to tag the latest version as $IMAGE:$NEXT_TAG."
        exit 1
    fi
    echo "Newer version updated, tagged, and pushed as: $IMAGE:$NEXT_TAG"
fi


