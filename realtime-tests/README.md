# Realtime Tools Container

This container provides a suite of tools for debugging and evaluating realtime performance, including `realtime-tests`, `rteval`, `rtla`, and `stress-ng`. It is designed to help users analyze scheduling latencies, interrupt latencies, and overall system responsiveness under different configurations, all within a reproducible and consistent containerized environment.

---

## Features

- **Realtime Performance Evaluation**: Tools to assess scheduling and interrupt latencies.
- **Consistency**: Reproducible results independent of the host system's configuration.
- **Interactive Shell**: Configure and run tests interactively.
- **Comprehensive Tools**:
  - `realtime-tests`
  - `rteval`
  - `rtla`
  - `stress-ng`

---

## Prerequisites

- A system with `podman` installed for running the container.

---

## Installation and Usage

### Pull and Run the Container

To run the container from Quay:
```bash
sudo podman run -it --rm --privileged quay.io/rts-container-tooling/realtime-tools:latest